﻿using System;

namespace ex_16
{
    class Program
    {
        static void Main(string[] args)
        {
           
           Console.WriteLine("please type your name");
           var name = Console.ReadLine();

           Console.WriteLine("*******************************");
           Console.WriteLine("====   Welcome to my app   ====");
           Console.WriteLine("*******************************");
           Console.WriteLine($"          {name}              ");
           Console.WriteLine("*******************************");

           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();
        }

    }
}
